import vkbottle
from vkbottle import Keyboard, KeyboardButtonColor, Text


name = ["Adidas", "Nike"]
color = ["Red", "Green", "Blue"]
size = ["7", "8", "9", "10"]


kb_name = (
    Keyboard(one_time=True, inline=False)
    .schema(
        [
            [
                {"label": name[0], "type": "text"},
            ],
            [
                {"label": name[1], "type": "text"}],
            ]
    )
    .get_json()
    )


kb_color = (
    Keyboard(one_time=True, inline=False)
    .schema(
        [
            [
                {"label": color[0], "type": "text"},
            ],
            [
                {"label": color[1], "type": "text"},
            ],
            [
                {"label": color[2], "type": "text"},
            ]
        ]
    )
    .get_json()
    )


kb_size = (
    Keyboard(one_time=True, inline=False)
    .schema(
        [
            [
                {"label": size[0], "type": "text"},
            ],
            [
                {"label": size[1], "type": "text"},
            ],
            [
                {"label": size[2], "type": "text"},
            ],
            [
                {"label": size[3], "type": "text"},
            ]
        ]
    )
    .get_json()
    )


kb_admin = (
    Keyboard(one_time=True, inline=False)
    .schema(
        [
            [
                {"label": "Добавить", "type": "text", "color": "positive"},
                {"label": "Редактировать", "type": "text"},
                {"label": "Удалить", "type": "text", "color": "negative"},
            ],
            [
                {"label": "Посмотреть склады", "type": "text"},
            ]
        ]
    )
    .get_json()
    )


kb_cancel = (
    Keyboard(one_time=True, inline=False)
    .schema(
        [
            [
                {"label": "Отменить", "type": "text"},
            ]
        ]
    )
    .get_json()
    )