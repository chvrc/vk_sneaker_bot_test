import vkbottle
from vkbottle import EMPTY_KEYBOARD
from vkbottle.bot import Bot, Message
from keyboards import kb_name, kb_color, kb_size, kb_admin, kb_cancel
from states import Search, Add, Admin


BOT_TOKEN = "e534a8747519a4fc2beb46abb7be84a2920c1287075a4c1de562768ef629fd00b69507fe32b7994fb511b"
admin_id = 0


bot = Bot(BOT_TOKEN)

arr = []


# Поиск пары
@bot.on.message(text="!найти")
async def search_start(message: Message):
    await bot.state_dispenser.set(message.peer_id, Search.name)
    await message.answer(message="Кнопки будут созданы автоматически по данным из базы\n-----\nВыберите модель:", keyboard=kb_name)

@bot.on.message(state=Search.name)
async def search_name(message: Message):
    arr.append(message.text)
    await bot.state_dispenser.set(message.peer_id, Search.color)
    await message.answer(message="Выберите расцветку:", keyboard=kb_color)

@bot.on.message(state=Search.color)
async def search_color(message: Message):
    arr.append(message.text)
    await bot.state_dispenser.set(message.peer_id, Search.size)
    await message.answer(message="Выберите размер:", keyboard=kb_size)

@bot.on.message(state=Search.size)
async def search_color(message: Message):
    arr.append(message.text)
    await bot.state_dispenser.set(message.peer_id, Search.price)
    await message.answer(message="Введите цену:")

@bot.on.message(state=Search.price)
async def search_size(message: Message):
    arr.append(message.text)
    answer = "Ищем совпадение в базе (%s - %s - %s)\n-----\n" % (arr[0], arr[1], arr[2])
    answer += "Сообщение, которое будет отправлено всем, у кого найдено совпадение:\n"
    answer += "Модель: %s\nРасцветка: %s\nРазмер:%s\nЦена:%s\nСсылка на профиль ВК:%s"  % (arr[0], arr[1], arr[2], arr[3], "https://vk.com/id0")
    await bot.state_dispenser.delete(message.peer_id)
    await message.answer(message=answer, keyboard=EMPTY_KEYBOARD)
    arr.clear()


# Создание склада
@bot.on.message(text="!добавить")
async def add_new(message: Message):
    await bot.state_dispenser.set(message.peer_id, Add.name)
    await message.answer(message="Выберите модель:", keyboard=kb_name)

@bot.on.message(state=Add.name)
async def add_name(message: Message):
    arr.append(message.text)
    await bot.state_dispenser.set(message.peer_id, Add.color)
    await message.answer(message="Выберите расцветку:", keyboard=kb_color)

@bot.on.message(state=Add.color)
async def add_color(message: Message):
    arr.append(message.text)
    await bot.state_dispenser.set(message.peer_id, Add.size)
    await message.answer(message="Выберите размер:", keyboard=kb_size)

@bot.on.message(state=Add.size)
async def add_color(message: Message):
    arr.append(message.text)
    await bot.state_dispenser.set(message.peer_id, Add.price)
    await message.answer(message="Введите цену:")

@bot.on.message(state=Add.price)
async def add_size(message: Message):
    arr.append(message.text)
    answer = "Создана запись (%s - %s - %s - %s)" % (arr[0], arr[1], arr[2], arr[3])
    await bot.state_dispenser.delete(message.peer_id)
    await message.answer(message=answer, keyboard=kb_cancel)
    arr.clear()


# Админ-панель
@bot.on.message(text="!админ")
async def admin_start(message: Message):
    await bot.state_dispenser.set(message.peer_id, Admin.start)
    await message.answer(message="Здесь выводится база данных в формате ID - NAME - COLOR\n-----\nВыберите действия", keyboard=kb_admin)

@bot.on.message(text="Добавить", state=Admin.start)
async def admin_add(message: Message):
    await bot.state_dispenser.set(message.peer_id, Admin.add)
    await message.answer(message="Введите модель и расцветку в формате NAME, COLOR:")

@bot.on.message(text="Редактировать", state=Admin.start)
async def admin_edit(message: Message):
    await bot.state_dispenser.set(message.peer_id, Admin.edit)
    await message.answer(message="Введите ID и новые данные в формате ID, NAME, COLOR:")

@bot.on.message(text="Удалить", state=Admin.start)
async def admin_delete(message: Message):
    await bot.state_dispenser.set(message.peer_id, Admin.delete)
    await message.answer(message="Введите ID:")

@bot.on.message(text="Посмотреть склады", state=Admin.start)
async def admin_delete(message: Message):
    await bot.state_dispenser.set(message.peer_id, Admin.warehouses)
    await message.answer(message="Введите ID:")

@bot.on.message(state=Admin.start)
async def admin_error(message: Message):
    await message.answer(message="Ошибка! Выберите одну из кнопок")

@bot.on.message(state=Admin.add)
async def admin_add_action(message: Message):
    await bot.state_dispenser.delete(message.peer_id)
    await message.answer(message="Запись добавлена (%s)" % message.text)

@bot.on.message(state=Admin.edit)
async def admin_edit_action(message: Message):
    await bot.state_dispenser.delete(message.peer_id)
    await message.answer(message="Запись изменена (%s)" % message.text)

@bot.on.message(state=Admin.delete)
async def admin_delete_action(message: Message):
    await bot.state_dispenser.delete(message.peer_id)
    await message.answer(message="Запись удалена (%s)" % message.text)

@bot.on.message(state=Admin.warehouses)
async def admin_delete_action(message: Message):
    await bot.state_dispenser.delete(message.peer_id)
    await message.answer(message="Здесь будет выведены все склады пользователей в формате\nMODEL-COLOR-SIZE-PRICE-VK_ID")



if __name__ == "__main__":
    bot.run_forever()

