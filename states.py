from vkbottle_types import BaseStateGroup


class Admin(BaseStateGroup):
    start = 0
    name = 1
    color = 2
    add = 3
    edit = 4
    delete = 5
    warehouses = 6


class Search(BaseStateGroup):
    name = 10
    color = 11
    size = 12
    price = 13


class Add(BaseStateGroup):
    name = 20
    color = 21
    size = 22
    price = 23